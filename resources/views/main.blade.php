<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ $main->content['en']['title'] }}</title>
	<link rel="shortcut icon" href="/favicon.ico">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/css/styles.css">
</head>

<body>

	<header>
		<h2><a href="#">{{ $main->content['en']['logo'] }}</a></h2>
	</header>


	<section class="hero">
		<div class="background-image" style="background-image: url({{ $imagePath }});"></div>
		<h1>{{ $main->content['en']['name'] }}</h1>
		<span>{!! $main->content['en']['body'] !!}</span>
	</section>


	<section class="our-work">
		<h3 class="title">{{ $gallery->content['en']['name'] }}</h3>
		<span>{!! $gallery->content['en']['body'] !!}</span>
		<hr>

		<ul class="grid">
			@foreach ($imagesGallery as $image)
				@if ((floor($loop->index / 2) & 1) && ($loop->index & 1))
					<li class="small" style="background-image: url({{ $image->path_image }} );"></li>
				@elseif (!(floor($loop->index / 2) & 1) && !($loop->index & 1))
					<li class="small" style="background-image: url({{ $image->path_image }} );"></li>
				@else
					<li class="large" style="background-image: url({{ $image->path_image }} );"></li>
				@endif				
			@endforeach
		</div>
	</section>

	<section class="contact">
		<h3 class="title">{{ $contact->content['en']['name'] }}</h3>	
		<span>{!! $contact->content['en']['body'] !!}</span>
		<hr>

		<form action="#" id="form-message">
			<div class="cst-col cst-col-left">
				<input type="text" placeholder="Имя" name="contact-name" id="input_name" required>
			</div>
			<div class="cst-col cst-col-right">
				<input type="email" placeholder="E-mail" name="contact-email"  id="input_email" required>
			</div>
			<div class="cst-clear"></div>
			<div class="cst-col" style="width: 100%;">
				<textarea placeholder="Комментарий" name="textarea-message" id="textarea_message" required></textarea>
			</div>
			<div class="cst-col cst-container-btn">
				<button id="btn_submit" name="submit" class="cst-btn">отправить</button>
			</div>
		</form>

	</section>

	<footer>
		<p>{!! $footer->content['en']['description'] !!}</p>
	</footer>
</body>
<script src="/js/jquery-1.8.3.js"></script>
<script>
	$( document ).ready(function() {

		$.ajaxSetup({
			headers: {
			  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});

		$( "#btn_submit" ).on( "click", function(event) {
			if ($("#form-message")[0].checkValidity()) {
				event.preventDefault();
				var mydata = $("form").serialize();
				$.ajax({
					type: "POST",
					url: "/send",
					data: mydata,
					success: function(data) {
						$("#input_name").val("");
						$("#input_email").val("");
						$("#textarea_message").val("");
						alert('E-mail успешно отправлен!');	
					},
					error: function(xhr, textStatus, errorThrown) {
						try {
							if (xhr.status === 422) {
								let responseText = JSON.parse(xhr.responseText);
								let message = '';						
								for (let fields in responseText.errors) {
									for (let field of responseText.errors[fields]) {
										message += field + '\n\r';
									}
								}
								alert(message);
							} else {
								alert(textStatus);
							}
						} catch (err) {
							alert('Ошибка на стороне сервера');
						}
					}
				});
			}
			return true;
		});

	});
</script>
</html>
