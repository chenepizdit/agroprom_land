<?php

namespace App\Orchid\Widgets;

use Orchid\Widget\Widget;
use Orchid\Screen\Field;

class UploadImage extends Field {

    /**
     * @var string
     */
    public $view = 'cstuploadimage';

    /**
     * Attributes available for a particular tag.
     *
     * @var array
     */
    public $inlineAttributes = [];

    /**
     * @param null $name
     * @return PictureField
     */
    public static function make($name = null): self
    {
        return (new static)->name($name);
    }

}
