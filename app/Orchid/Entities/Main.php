<?php

declare(strict_types=1);

namespace App\Orchid\Entities;

use Orchid\Screen\Field;
use Orchid\Press\Entities\Single;
use Orchid\Screen\Fields\MapField;
use Orchid\Screen\Fields\UTMField;
use Orchid\Screen\Fields\CodeField;
use Orchid\Screen\Fields\TagsField;
use Orchid\Screen\Fields\InputField;
use Orchid\Screen\Fields\QuillField;
use Orchid\Screen\Fields\UploadField;
use Orchid\Screen\Fields\SelectField;
use Orchid\Screen\Fields\PictureField;
use Orchid\Screen\Fields\TinyMCEField;
use Illuminate\Database\Eloquent\Model;
use Orchid\Screen\Fields\CheckBoxField;
use Orchid\Screen\Fields\TextAreaField;
use Orchid\Screen\Fields\DateTimerField;
use Orchid\Screen\Fields\SimpleMDEField;
use Orchid\Screen\Fields\RelationshipField;
use App\Orchid\Widgets\UploadImage;

class Main extends Single
{
    /**
     * @var string
     */
    public $name = 'Главная';

    /**
     * @var string
     */
    public $description = 'Редактирование первого слайда';

    /**
     * @var string
     */
    public $slug = 'main';

    /**
     * Slug url /news/{name}.
     *
     * @var string
     */
    public $slugFields = 'name';

    /**
     * Menu group name.
     *
     * @var null
     */
    public $groupname = null;

    /**
     * Rules Validation.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'id' => 'sometimes|integer|unique:posts',
            'content.*.name' => 'required|string',
            'content.*.logo' => 'required|string',
            'content.*.title' => 'required|string',
        ];
    }

    /**
     * @return array
     * @throws \Throwable|\Orchid\Screen\Exceptions\TypeException
     */
    public function fields(): array
    {
        return [

            Field::group([

                InputField::make('logo')
                    ->type('text')
                    ->max(255)
                    ->required()
                    ->title('Логотип')
                    ->help('Введите название логотипа'),

                InputField::make('title')
                    ->type('text')
                    ->max(255)
                    ->required()
                    ->title('Заголовок сайта')
                    ->help('Заголовок html-документа, тайтл'),

            ]),

            InputField::make('name')
                    ->type('text')
                    ->max(255)
                    ->required()
                    ->title('Название сайта')
                    ->help('Введите название сайта'),

            TinyMCEField::make('body')
                ->required()
                ->title('Описание сайта')
                ->help('Введите описание сайта')
                ->theme('modern'),
            
        ];
    }

    /**
     * @return array
     * @throws \Orchid\Screen\Exceptions\TypeException
     * @throws \Throwable
     */
    public function main(): array
    {
        return array_merge(parent::main(), [
            UploadImage::make('attachment'),
        ]);
    }

    /**
     * @param \Illuminate\Database\Eloquent\Model $model
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(Model $model) : Model
    {
        return $model->load(['attachment', 'tags', 'taxonomies'])
            ->setAttribute('category', $model->taxonomies->map(function ($item) {
                return $item->id;
            })->toArray());
    }

    /**
     * @param \Illuminate\Database\Eloquent\Model $model
     */
    public function save(Model $model)
    {   
        $model->save();

        $model->taxonomies()->sync(array_flatten(request(['category'])));
        $model->setTags(request('tags', []));
        $model->attachment()->sync(request('attachment', []));
    }

    /**
     * @return array
     * @throws \Throwable
     */
    public function options(): array
    {
        return [];
    }
}
