<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Orchid\Press\Models\Post;
use DB;
use Mail;
use App\Http\Requests\MailRequest;

class AppController extends Controller
{
    public function index() {
        $main = Post::where('slug', 'main')->first();

        $imageMain = DB::table('attachments')
            ->join('attachmentable', 'attachmentable.attachment_id', '=', 'attachments.id')
            ->where('attachmentable.attachmentable_id', '=', $main->id)
            ->first(); 

        $image = $imageMain->path_image = '/storage/' . $imageMain->path . 
            '/' . $imageMain->name . '.' . $imageMain->extension;
        
        $gallery = Post::where('slug', 'gallery')->first();
        
        $imagesGallery = DB::table('attachments')
            ->join('attachmentable', 'attachmentable.attachment_id', '=', 'attachments.id')
            ->where('attachmentable.attachmentable_id', '=', $gallery->id)
            ->get(); 

        $images = $imagesGallery->map(function ($item, $key) {
            $item->path_image = '/storage/' . $item->path . '/' . $item->name . '.' . $item->extension;
            return $item;
        });
        
        $contact = Post::where('slug', 'contact')->first();

        $footer = Post::where('slug', 'footer')->first();
  
        return view('main', [
            'main' => $main,
            'imagePath' => $image,
            'gallery' => $gallery,
            'imagesGallery' => $images,
            'contact' => $contact,
            'footer' => $footer,
        ]);
    }

    public function send(MailRequest $request) {
        $this->sendMail(
            $request['contact-name'], 
            $request['contact-email'],
            $request['textarea-message']
        );
    }

    public function sendMail($name, $email, $comment) {

        $toName = env('MAIL_NAME');
        $toEmail = env('MAIL_USERNAME');
        $fromEmail = env('MAIL_USERNAME');

        $data = [
            'name' => $name,
            'email' => $email,
            'comment' => $comment
        ]; 
        
        Mail::send('emails.mail', $data, function($message) use ($toName, $toEmail, $fromEmail) {
            $message->to($toEmail, $toName)
                    ->subject('ДОПЭТАЛОН - обратная связь');
            $message->from($fromEmail, 'Обратная связь');
        });

    }
}
