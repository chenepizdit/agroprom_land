<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'contact-name' => 'required',
            'contact-email' => 'required|email',
            'textarea-message' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'contact-name.required' => 'Поле "Имя" обязательно для заполнения',
            'contact-email.required'  => 'Поле "E-mail" обязательно для заполнения',
            'contact-email.email'  => 'Поле "E-mail" имеет неверный формат',
            'textarea-message.required'  => 'Поле "комментарий" обязательно для заполнения',
        ];
    }
}
